#!/usr/bin/env python3
# coding: utf-8

# My least poorly programmed thing ever.

# We need googletrans for the Google Translate Api
from googletrans import Translator  # This import supplies the actual function we will use to talk to the api.
from googletrans import LANGUAGES  # Importing this defines a dictionary called LANGAUGES containing each code's name.

# We need a way to serve a bunch of different proxies through a local ip.
from proxybroker import Broker  # This grabs free proxies from a bunch of different sources and can serve them.

# These will be helpful for...
import warnings  # ...warning.
import random  # ...randomizing.
import time  # ... timing.
import json  # ... configuration and saving results.
import sys  # ...call system functions.
from pathlib import Path  # ...paths.
import click  # ...command line interface.
import tkinter
import asyncio


def load_config():  # Define the function for loading the config json file.

    # Define paths
    config_path = Path(Path.cwd() / 'config/config.json')
    default_config_path = Path(Path.cwd() / 'config/defaults.json')

    if config_path.is_file():  # If the config file exists and is not a dir.

        with config_path.open(mode='r') as config_file:  # Open the config file.
            config = json.load(config_file)  # Load it to a dictionary.

            if default_config_path.is_file() is False:  # If the default config does not exist or is a dir.
                with default_config_path.open(mode='w') as default_config_file:  # Open defaults for write.

                    # Warn the user.
                    warnings.warn("\nDefault config file missing.")
                    warnings.warn("To remedy this we copied the current user config to the default one.")
                    warnings.warn("Please re-download the actual default config next chance you get.")

                    default_config_file.write(config_file.read())  # Write the current config to avoid issues.

    elif default_config_path.is_file():  # If the default config file does exist and is file, but the user one does not.

        with default_config_path.open(mode='r') as default_config_file:  # Open the default config file to read.

            with config_path.open(mode='w') as config_file:  # Open the config file to write.
                config_file.write(default_config_file.read())  # Write the default config file to the actual config.

            config = json.load(default_config_file)  # Load it to a dictionary.

    else:  # This should never happen, but be prepared for it anyway.

        # Warn the user.
        warnings.warn("\nCannot find the config file!")
        warnings.warn("Please make sure that the 'config' folder exists and contains 'default.json' or 'config.json'.")

        sys.exit()  # We can't salvage this yet, so end the program here.

    return config  # Return the config dictionary.


load_config()  # Make sure the config loads correctly. This function handles its own exceptions.


def setup():  # We are using more async this time around. This will make the discord bot easier.

    config = load_config()  # Load the config to a variable.

    try:  # Test starting a proxy server to see if the port is open
        test_broker = Broker()  # Make a test broker.
        test_broker.serve(host=config['Proxy']['Host'], port=config['Proxy']['Port'])  # Start the server.

    except OSError as e:  # Oh no! That port is in use already! We should let the user know so they can fix it.

        # Warn the user.
        warnings.warn(e)
        warnings.warn("\nCannot start the proxy server on port "+str(config['Proxy']['Port'])+".")
        warnings.warn(
            "Please make there are sure no processes, like another proxy server, running on that port before "
            "continuing.")

        sys.exit()  # End the script here.

    finally:  # Finally, if the above didn't crash and burn.

        # Destroy the test broker.
        test_broker.stop()  # Stop the test broker and server.
        test_broker = None  # Set it to none.
        del test_broker  # Delete it completely.

        proxy_broker = Broker()  # Make the real broker and server.
        proxy_broker.serve(types=list(config['Proxy']['Types']), host=config['Proxy']['Host'],
                                 port=config['Proxy']['Port'], limit=config['Proxy']['Limit'])  # Start serving.


# The following functions are parts of the obfuscate function modified and split into separate, small, functions.
# This is done so that they can be called and modified separately in alternate obfuscate functions.
# Any lines of code that do not change between different obfuscate functions go here.
# The ony exception being lines that solely set up variables and nothing else.


def googtrans_urls():  # This function pulls up the country mirrors.

    google_trans_url_file = Path('./config/translate_urls.txt')

    # Load a list of all Google Translate country urls. This should contain every known mirror of translate.google.com.
    with google_trans_url_file.open(mode='r') as text_file:  # Open a file of translate urls for reading.
        translate_urls = text_file.read().splitlines()  # Read each line to a string in a list.

    for x in range(0, 5):  # Loop the following code 5 times.
        random.shuffle(translate_urls)  # Shuffle the Google Translate urls. The first one is usually the one used.

    return translate_urls  # Return the list.


def lang_name(code):  # This function takes the code of a language and spits out the name of the language.
    return ' '.join([x.title() for x in LANGUAGES[code.lower()].split(' ')])  # Return capitalized language names.


def iteration_names(mid_order):  # This function appends the names of all middle iteration languages to a list.

    lang_iterations = []  # Make a empty list to store these language's names for use in the results dictionary.

    for x in mid_order:  # Loop the following code for every language code in mid_order.
        # Define the current code as 'x'.
        lang_iterations.append(lang_name(x))  # Append names of entries in mid_order.

    return lang_iterations  # Return the list back.


def save_results(save, results):  # This function saves the results dictionary.
    if save:  # If the user wants to save the results of the translation.

        results_dir = Path('results/')

        saved_results = len(list(results_dir.glob('*')))  # Count files.

        current_result = str(saved_results+1)+'.json'  # Set the current result filename to the file count plus one.

        current_result_path = results_dir / current_result

        with current_result_path.open(mode='w') as outfile:  # Open the result json to write.
            json.dump(results, outfile, indent=4, sort_keys=False)  # Dump the json to the file and close the file.


# Here begins the actual obfuscate functions.


def obfuscate(src_text, src, dest, mid_order, wait=load_config()['Defaults']['Wait'],
              save=load_config()['Defaults']['Save']):  # This is the regular obfuscate function.

    if dest.lower() == 'auto':  # If the user sets the destination to auto.
        warnings.warn("The destination language cannot be automatically detected, dumbass!")  # Tell the user off.
        sys.exit()  # End the script here.

    config = load_config()  # Load the config to a variable.

    proxy_broker = Broker()  # Make the proxy broker and server.
    proxy_broker.serve(types=list(config['Proxy']['Types']), host=config['Proxy']['Host'],
                       port=config['Proxy']['Port'], limit=config['Proxy']['Limit'])  # Start serving.

    translate_urls = googtrans_urls()  # Load the urls to a variable.

    # Proxy Dictionary. All of these are the same local address, but with different protocols.
    translate_proxies = config['Translate']['Proxies']  # These are grabbed from the config file.

    mis_translator = Translator(service_urls=translate_urls, proxies=translate_proxies)  # Define the translator.

    # Set up variables for the loop.
    src_name = lang_name(src)  # Save the source language name.
    dest_name = lang_name(dest)  # Save the destination language name.
    text_iterations = []  # Save the text from every middle iteration in a list.

    last_dest = src.lower()  # Set the last destination to the first source language.
    last_dest_name = src_name  # Set the last destination's name to the source language name.
    last_text = src_text  # Set the last output text to our source text.

    sep = "\n-----------------------------------------------------------------------------------------------"

    # This is the first iteration.
    first_iter = True
    cur_iter = 1

    for x in mid_order:  # Loop the following code for every language code in mid_order. Define the...
        # ...current code as 'x'.

        # Variable set-up for each individual iteration.
        iter_code = x.lower()  # Record our current dest iteration code and format it as lowercase.
        iter_name = lang_name(iter_code)  # Record the dest language name of the current iteration.

        # Print a separator.
        print(sep)

        if cur_iter == len(mid_order):  # If this is the last iteration.
            print("--Final Iteration--")  # Print that this is the last iteration.

        # Start printing output to the screen.

        # Print iteration.
        print("Iteration "+str(cur_iter)+": Translating from "+last_dest_name+" to "+iter_name+".")
        print(last_dest_name+": "+'"'+last_text+'"')  # Print the last translation result.

        trans = mis_translator.translate(last_text, src=last_dest, dest=iter_code)  # Preform the actual translation.

        # Set variables for next loop.
        last_dest = trans.dest  # Set the last destination so it can be used as the next source.
        last_dest_name = lang_name(x)  # Set the name of the last destination.
        last_text = trans.text  # Save the result of the translation for later use.
        text_iterations.append(trans.text)  # Save the result in a list for later use.

        print(last_dest_name+": "+'"'+last_text+'"')  # Print the resulting text for this translation.

        time.sleep(wait)  # Wait the specified amount of time so we are not spamming Google's servers.

        if cur_iter == len(mid_order):  # If this is the last iteration, translate to the destination language.
            print(sep+"\nTranslating to Destination Language"+" (Iteration "+str(cur_iter+1)+")"+": Translating from "
                  ""+last_dest_name+" to "+dest_name+".")  # Describe iteration and print separator.
            print(last_dest_name+": "+'"' + last_text+'"')  # Print the last translation result.

            trans = mis_translator.translate(last_text, src=last_dest, dest=dest.lower())  # Run the translation.

            print(dest_name+": "+'"'+trans.text+'"'+sep)  # Print the resulting text for this translation and a sep.

            dest_text = trans.text  # Save the final translation to a variable for consistency's sake.

            lang_iterations = iteration_names(mid_order)  # Save the middle iteration language names to a variable.

            # Save the results to a dictionary.
            # At this point we need to convert our lists into tuples because python dictionaries do not accept lists.
            results = {'Source Language': src_name, 'Destination Language': dest_name,
                       'Source Language Code': src.lower(), 'Destination Language Code': dest.lower(),
                       'Source Text': src_text, 'Destination Text': dest_text, 'Middle Iterations': len(mid_order),
                       'Middle Language Iteration Order': tuple(lang_iterations),
                       'Middle Iteration Order': tuple(mid_order), 'Middle Iteration History': tuple(text_iterations)}

            save_results(save, results)  # Handle saving the results if necessary.

        # This is here to refresh the Google Translate token enough to hide from google's bans on the current proxy.
        if cur_iter % 2 == 0:  # If current iteration is even
            mis_translator = None  # Clear the translator variable.
            del mis_translator  # Delete the translator variable.
            mis_translator = Translator(service_urls=translate_urls, proxies=translate_proxies)  # Reset the translator.

        cur_iter = cur_iter+1  # Increment current iteration.
    proxy_broker.stop()


# This is the random obfuscate function.
def obfuscate_random(src_text, src, dest, iterations, wait=load_config()['Defaults']['Wait'],
                     save=load_config()['Defaults']['Save'], allow_repeats=True,
                     all_langs=load_config()['Defaults']['All Langs'],
                     lang_list=['This is a placeholder.']):  # This is the random obfuscate function.

    config = load_config()  # Load the config to a variable.

    if all_langs:  # If we are allowed to pick from every language supported.
        langs = list(LANGUAGES)  # Print all language codes google translate accepts into a list.
        LANGUAGES_LIST = langs  # Print all language codes google translate accepts into a different list.

    elif all_langs is False and len(lang_list) <= iterations and allow_repeats is False:  # If we are not allowed to...
        # pick from any language supported, but we have no valid list or there are not enough languages to pick from.

        # We must have more languages to choose from than iterations otherwise calling this function is pointless...
        # ...and wasteful.

        # Warn the user of this stupid mistake they made.
        warnings.warn("No valid list of usable languages was provided, or not nearly enough were provided.")
        warnings.warn("This is a contradiction or just pain stupidity. Please provide a valid list or let us use all"
                      " languages.")

        sys.exit()  # End the script here.

    else:  # If we are not allowed to pick from all languages and the user input is not fucked up.
        langs = lang_list  # Use the list provided to us by the user or preset order selector function.
        LANGUAGES_LIST = list(LANGUAGES)  # Print all language codes google translate accepts into a different list.

    random.shuffle(langs)  # Shuffle the list for extra randomness.

    if iterations > len(langs) and allow_repeats is False:  # If there are more iterations than langs and repeats are...
        # ...disabled.
        # Throw an exception here.
        pass

    if src.lower() != 'auto':  # If the source language is not set to be automatically detected.
        langs.remove(src.lower())  # Remove the source language from the list of choices.
    if src.lower() != dest.lower() and dest.lower() != 'auto':  # If the destination is not equal to the source or auto.
        langs.remove(dest.lower())  # Remove that, too.

    mid_order = []  # Set up the mid order list.

    for x in range(0, iterations):  # For every iteration number.
        random_lang = random.choice(langs)  # Pick a random language code.

        while allow_repeats is False and mid_order != [] and random_lang in mid_order:  # While repeats are disabled...
            # ... and the current choice is already in mid_order and mid_order is not empty.1
            langs.remove(random_lang)  # Remove that option from our choices.
            random_lang = random.choice(langs)  # Make a different choice and loop this until we have a non-repeat.

        mid_order.append(random_lang)  # Add the random choice to the middle iteration order.
        random.shuffle(mid_order)  # Shuffle the middle iteration order for extra randomness.

    obfuscate(src_text, src, dest, mid_order, wait, save)  # Run the original obfuscate function with our new inputs.


@click.group()
def main():
    """This version of the cli takes the source text, src/dst codes, wait time, and middle iteration order/number.
    This function requires four arguments in total. This the obfuscate function.

    \b
    * This is a very early demo, and will be unmaintained unless breaking bugs are found.
    ** The above means user error will not be fixed/filtered/autocorrected for this release, but for the completed release.

    The config and results folders are stored in the current working directory. This will be optional in the future.

    Also, for a side note, the discord bot will share the same syntax as the final version of this cli.
    """
    pass

@main.command()
@click.option('--doof/-no-doof', default=False, hidden=True)
def gui(doof):
    """
    Run the much more user-friendly Graphical version of the script.
    This function has evil pharmacist powers.
    """

    # Create the main window and give it a title.
    root = tkinter.Tk()
    if doof:
        root.title("The Py Obfuscator-inator")
        quit_button_sting = "Self-destruct Button"
    else:
        root.title("Py Obfuscator")
        quit_button_sting = "Quit"

    op = tkinter.OptionMenu(root, options=['d', 's', 'c', 'f'])
    op.pack
    l = tkinter.Label(text=op.selection_get(), font=("Helvetica", 20))
    l.pack()
    b = tkinter.Button(text=quit_button_sting, command=sys.exit)
    b.pack()

    root.mainloop()  # Run until program exit.


@main.command()
@click.argument('src_txt', nargs=1, type=click.STRING)
@click.argument('src', nargs=1, type=click.STRING)
@click.argument('dst', nargs=1,type=click.STRING)
@click.argument('wait', nargs=1, type=click.INT)
@click.argument('mid_order', nargs=-1)
def obfs(src_txt, src, dst, wait, mid_order):
    """This the obfuscate function.

    \b
    These are defined as follows:
    SRC_TEXT = The text you want to feed the obfuscator. This should be one line and enclosed in double quotes.
    SRC = Source language code, usually you want this to be English (en) or Auto (auto).
    DST =  Destination language code, usually you want English (en).
    WAIT = A integer that takes how many SECONDS to wait between iterations. I recommend 5 for 15 iterations and below.
    MID_ORDER = Takes one integer or infinite language codes in quotes.


    * Integers should NOT be in quotes.
    """

    obfuscate(src_txt, src, dst, list(mid_order), wait=wait)


@main.command()
@click.argument('src_txt', nargs=1, type=click.STRING)
@click.argument('src', nargs=1, type=click.STRING)
@click.argument('dst', nargs=1,type=click.STRING)
@click.argument('wait', nargs=1, type=click.INT)
@click.argument('iterations', nargs=1, type=click.INT)
def rand(src_txt, src, dst, wait, iterations):
    """This the random obfuscate function.

    \b
    These are defined as follows:
    SRC_TEXT = The text you want to feed the obfuscator. This should be one line and enclosed in double quotes.
    SRC = Source language code, usually you want this to be English (en) or Auto (auto).
    DST =  Destination language code, usually you want English (en).
    WAIT = A integer that takes how many SECONDS to wait between iterations. I recommend 5 for 15 iterations and below.
    ITERATIONS = Takes one integer for how many iterations you want.


    * Integers should NOT be in quotes.
    """

    obfuscate_random(src_txt, src, dst, iterations, wait=wait)


if __name__ == "__main__":
    main()